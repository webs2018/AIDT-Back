export default {
  /**
	 * @description token在Cookie中存储的天数，默认1天
	 */
  // cookieExpires: 1,
  /**
	 * @description 是否使用国际化，默认为false
	 *              如果不使用，则需要在路由中给需要在菜单中展示的路由设置meta: {title: 'xxx'}
	 *              用来在菜单中显示文字
	 */
  useI18n: false,
  /**
	 * @description api请求基础路径
	 */
  baseUrl: {
    dev: 'https://www.easy-mock.com/mock/5add9213ce4d0e69998a6f51/iview-admin/',
    // pro: 'http://192.168.0.7:8098'
    pro: 'https://admin.aidtvip.com'
    //				pro: 'http://192.168.0.104:8098'
    //				pro: 'http://192.168.0.168:8098'
    //  pro: 'http://192.168.0.179:28099/admin'
  }
}
