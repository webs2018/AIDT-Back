import Main from '@/view/main'
import parentView from '@/components/parent-view'

/**
 * iview-admin中meta除了原生参数外可配置的参数:
 * meta: {
 *  hideInMenu: (false) 设为true后在左侧菜单不会显示该页面选项
 *  notCache: (false) 设为true后页面不会缓存
 *  access: (null) 可访问该页面的权限数组，当前路由设置的权限会影响子路由
 *  icon: (-) 该页面在左侧菜单、面包屑和标签导航处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 * }
 */

export default [{
		path: '/login',
		name: 'login',
		meta: {
			title: 'Login - 登录',
			hideInMenu: true
		},
		component: () =>
			import('@/view/login/login.vue')
	}, {
		path: '/',
		component: Main,
		redirect: '/home',
		name: '',
		meta: {
			hideInMenu: true,
			notCache: true,
		},
		children: [{
			path: '/home',
			name: 'home',
			meta: {
				hideInMenu: true,
				title: '首页',
				notCache: true
			},
			component: () =>
				import('@/view/ATM/main/index')
		}]
	}, {
		path: '/accountManagement',
		meta: {
			icon: 'md-people',
			title: '账户管理'
		},
		name: 'accountManagement',
		component: Main,
		redirect: '/accountManagement/operationManagementAccount',
		children: [{
			path: 'operationManagementAccount',
			meta: {
				title: '运营管理账号'
			},
			name: 'operationManagementAccount',
			component: () =>
				import('@/view/ATM/accountManagement/operationManagementAccount')
		}, {

			path: 'authority',
			meta: {
				title: '角色管理'
			},
			name: 'authority',
			component: () =>
				import('@/view/ATM/accountManagement/authority')

		}, {
			path: 'memberList',
			meta: {
				title: '会员列表'
			},
			name: 'memberList',
			component: () =>
				import('@/view/ATM/accountManagement/memberList')
		}]
	},
	// {
	// 	path: '/transactionManagement',
	// 	meta: {
	// 		icon: 'ios-share-alt',
	// 		title: '交易管理'
	// 	},
	// 	name: 'transactionManagement',
	// 	component: Main,
	// 	redirect: '/transactionManagement/publishTransaction',
	// 	children: [{
	// 		path: 'publishTransaction',
	// 		meta: {
	// 			title: '发布交易'
	// 		},
	// 		name: 'publishTransaction',
	// 		component: () =>
	// 			import('@/view/ATM/transactionManagement/publishTransaction')
	// 	}, {
	// 		path: 'publishRecord',
	// 		meta: {
	// 			title: '发布记录'
	// 		},
	// 		name: 'publishRecord',
	// 		component: () =>
	// 			import('@/view/ATM/transactionManagement/publishRecord')
	// 	}, {
	// 		path: 'tradingFlow',
	// 		meta: {
	// 			title: '交易流水'
	// 		},
	// 		name: 'tradingFlow',
	// 		component: () =>
	// 			import('@/view/ATM/transactionManagement/tradingFlow')
	// 	}, ]
	// },
	{
		path: '/financialManagement',
		meta: {
			icon: 'md-trending-up',
			title: '财务管理'
		},
		name: 'financialManagement',
		component: Main,
		redirect: '/financialManagement/financeStatistics',
		children: [
			{
				path: 'withdrawalReview',
				meta: {
					title: '提现审核'
				},
				name: 'withdrawalReview',
				component: () =>
					import('@/view/ATM/financialManagement/withdrawalReview')
			},
			{
				path: '/financialManagement/moneyList',
				meta: {
					title: '充值列表'
				},
				name: 'moneyList',
				component: () =>
					import('@/view/ATM/financialManagement/moneyList')
			},
			{
				path: '/financialManagement/transferManagement',
				meta: {
					title: '转入列表'
				},
				name: 'transferManagement',
				component: () =>
					import('@/view/ATM/financialManagement/transferManagement')
			},
			// {
			// 	path: 'financeStatistics',
			// 	meta: {
			// 		title: '财务统计'
			// 	},
			// 	name: 'financeStatistics',
			// 	component: () =>
			// 		import('@/view/ATM/financialManagement/financeStatistics')
			// },
			{
				path: '/financialManagement/receivedManagement',
				meta: {
					title: '获赠列表'
				},
				name: 'receivedManagement',
				component: () =>
					import('@/view/ATM/financialManagement/receivedManagement')
			},
			{
				path: '/financialManagement/releaseList',
				meta: {
					title: '释放列表'
				},
				name: 'releaseList',
				component: () =>
					import('@/view/ATM/financialManagement/releaseList')
			},
			{
				path: '/financialManagement/directDrive',
				meta: {
					title: '直推列表'
				},
				name: 'directDrive',
				component: () =>
					import('@/view/ATM/financialManagement/directDrive')
			}
		]
	},
	// {
	// 	path: '/OTCAgent',
	// 	meta: {
	// 		icon: 'md-contact',
	// 		title: 'OTC'
	// 	},
	// 	name: 'OTCAgent',
	// 	component: Main,
	// 	redirect: '/OTCAgent/agentList',
	// 	children: [{
	// 			path: 'agentList',
	// 			meta: {
	// 				title: '商户列表'
	// 			},
	// 			name: 'agentList',
	// 			component: () =>
	// 				import('@/view/ATM/OTCAgent/agentList')
	// 		},
	// 		{
	// 			path: 'agentRecord',
	// 			meta: {
	// 				title: '明细订单'
	// 			},
	// 			name: 'agentRecord',
	// 			component: () =>
	// 				import('@/view/ATM/OTCAgent/agentRecord')
	// 		},
	// 		{
	// 			path: 'arbitrationManagement',
	// 			meta: {
	// 				title: '仲裁管理'
	// 			},
	// 			name: 'arbitrationManagement',
	// 			component: () =>
	// 				import('@/view/ATM/OTCAgent/arbitrationManagement')
	// 		},
	// 	]
	// },
	// {
	// 	path: '/queryStats',
	// 	meta: {
	// 		icon: 'ios-create',
	// 		title: '统计查询'
	// 	},
	// 	name: 'queryStats',
	// 	component: Main,
	// 	redirect: '/queryStats/giveCandy',
	// 	children: [{
	// 			path: 'giveCandy',
	// 			meta: {
	// 				title: '糖果赠送'
	// 			},
	// 			name: 'giveCandy',
	// 			component: () =>
	// 				import('@/view/ATM/queryStats/giveCandy')
	// 		},
	// 		{
	// 			path: 'releaseCandy',
	// 			meta: {
	// 				title: '糖果释放'
	// 			},
	// 			name: 'releaseCandy',
	// 			component: () =>
	// 				import('@/view/ATM/queryStats/releaseCandy')
	// 		},
	// 		{
	// 			path: 'dynamicIncome',
	// 			meta: {
	// 				title: '动态收益'
	// 			},
	// 			name: 'dynamicIncome',
	// 			component: () =>
	// 				import('@/view/ATM/queryStats/dynamicIncome')
	// 		},
	// 		{
	// 			path: 'weightedIncome',
	// 			meta: {
	// 				title: '加权奖励'
	// 			},
	// 			name: 'weightedIncome',
	// 			component: () =>
	// 				import('@/view/ATM/queryStats/weightedIncome')
	// 		},
	// 	]
	// },
	{
		path: '/operationalPromotion',
		meta: {
			icon: 'ios-keypad',
			title: '运营推广'
		},
		name: 'operationalPromotion',
		component: Main,
		redirect: '/operationalPromotion/bulletinManagement',
		children: [
			{
				path: 'bannerManagement',
				meta: {
					icon: '',
					title: '平台管理'
				},
				name: 'bannerManagement',
				component: () =>
					import('@/view/ATM/operationalPromotion/bannerManagement')
			},
			{
				path: 'bulletinManagement',
				meta: {
					icon: '',
					title: '公告管理'
				},
				name: 'bulletinManagement',
				component: () =>
					import('@/view/ATM/operationalPromotion/bulletinManagement')
			},
			{
				path: 'newsManagement',
				meta: {
					title: '资讯管理'
				},
				name: 'newsManagement',
				component: () =>
					import('@/view/ATM/operationalPromotion/newsManagement')
			}
		]
	},
	{
		path: '/parameterConfiguration',
		meta: {
			icon: 'ios-settings',
			title: '参数配置'
		},
		name: 'parameterConfiguration',
		component: Main,
		redirect: '/parameterConfiguration/feeConfiguration',
		children: [{
				path: 'feeConfiguration',
				meta: {
					title: '全局配置'
				},
				name: 'feeConfiguration',
				component: () =>
					import('@/view/ATM/parameterConfiguration/feeConfiguration')
			},
			{
				path: 'Release',
				meta: {
					title: '释放配置'
				},
				name: 'Release',
				component: () =>
					import('@/view/ATM/parameterConfiguration/Release')
			},
			// {
			// 	path: 'htmlConfiguration',
			// 	meta: {
			// 		title: '网站配置'
			// 	},
			// 	name: 'htmlConfiguration',
			// 	component: () =>
			// 		import('@/view/ATM/parameterConfiguration/htmlConfiguration')
			// },
			// {
			// 	path: 'levelConfiguration',
			// 	meta: {
			// 		title: '等级配置'
			// 	},
			// 	name: 'levelConfiguration',
			// 	component: () =>
			// 		import('@/view/ATM/parameterConfiguration/levelConfiguration')
			// },
			// {
			// 	path: 'incomeConfiguration',
			// 	meta: {
			// 		title: '动态收益配置'
			// 	},
			// 	name: 'incomeConfiguration',
			// 	component: () =>
			// 		import('@/view/ATM/parameterConfiguration/incomeConfiguration')
			// },
			// {
			// 	path: 'lineConfiguration',
			// 	meta: {
			// 		title: '区块链配置'
			// 	},
			// 	name: 'lineConfiguration',
			// 	component: () =>
			// 		import('@/view/ATM/parameterConfiguration/lineConfiguration')
			// },
			// {
			// 	path: 'otcConfiguration',
			// 	meta: {
			// 		title: 'OTC配置'
			// 	},
			// 	name: 'otcConfiguration',
			// 	component: () =>
			// 		import('@/view/ATM/parameterConfiguration/otcConfiguration')
			// },
			// {
			// 	path: 'grbcConfiguration',
			// 	meta: {
			// 		title: 'GRBC服务配置'
			// 	},
			// 	name: 'grbcConfiguration',
			// 	component: () =>
			// 		import('@/view/ATM/parameterConfiguration/grbcConfiguration')
			// },

		]
	},
	{
		path: '',
		meta: {
			icon: '',
			title: '操作日志'
		},
		name: '',
		component: Main,
		redirect: '/log',
		children: [{
			path: 'log',
			meta: {
				icon: 'md-reorder',
				title: '操作日志'
			},
			name: '操作日志',
			component: () =>
				import('@/view/ATM/log/log')
		}]
	},
]