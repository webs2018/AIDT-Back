export default {
  zhanghu: 'Account',
  yunying: 'Operating account',
  shenhe: 'To be audited',
  huiyuan: 'Membership list',
  caiwu: 'Financial management',
  tongji: 'Financial statistics',
  baodan: 'Report management',
  c2cgl: 'C2C management',
  ethcz: 'ETH recharge management',
  chaxun: 'Statistical query',
  shouyi: 'Revenue query',
  ethzz: 'ETH gift inquiry',
  yunyingtg: 'Promotion',
  gonggao: 'Announcement',
  lunbotu: 'Sowing map',
  zixun: 'Industry information',
  tousu: 'Complaint proposal',
  canshu: 'Parameter setting'
}
